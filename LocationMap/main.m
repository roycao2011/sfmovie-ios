//
//  main.m
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
