//
//  ViewController.m
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import "ViewController.h"
#define METERS_PER_MILE 1609.344

@interface ViewController ()
{
    NSMutableData *_responseData;            //raw data from server
    NSDictionary *_json;                     //address data in json
    NSDictionary *_json_google;              //location data in json
    NSMutableArray *addr_list;               //store addressed from server
    BOOL server_connection;
    NSNumber *lat;
    NSNumber *lng;
    int counter;
}
@end

@implementation ViewController
@synthesize mapView, film_name;

#pragma mark NSURLConnectionDataDelegate methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _responseData = [[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_responseData appendData:data];
}

-(NSCachedURLResponse*)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return nil;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    counter--;
    NSLog(@"get some url error\n");
    if (counter==0) {
        NSLog(@"ERROR ALERT\n");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Address cannot found!"
                                                        message:@"There is no location for this movie."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if (server_connection) {
        server_connection = NO;
        NSLog(@"get address_list from server\n");
        _json = [[NSDictionary alloc] init];
        _json = nil;
        if (_responseData) {
            _json = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:nil];
            addr_list = [NSMutableArray arrayWithArray:_json[@"result"]];
            counter = addr_list.count;
            if (counter==0) { //no address for this movie
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Address!"
                                                                message:@"There is no Address for this movie."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                return;
            }
            for (int n=0; n<addr_list.count; n++) {
                NSString *address = [addr_list objectAtIndex:n];
                NSLog(@"list[%d]=%@\n", n, [addr_list objectAtIndex:n]);
                NSString *urlAsString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@,+san+francisco,+CA&key=AIzaSyCBLL2U4fAvE6p3rC_j9KZqwOhrnPxCPcI", address];
                NSLog(@"url***%@\n", urlAsString);
                NSURL *url = [[NSURL alloc] initWithString:urlAsString];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                
                NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                if (!theConnection) {
                    NSLog(@"google connection failed\n");
                }
                NSLog(@"google good connection\n");
            }
            
            NSLog(@"get json data\n");
        }
    } else{
        //counter++;
        NSLog(@"get google map API data\n");
        _json_google = [[NSDictionary alloc] init];
        _json_google = nil;
        if (_responseData) {
            _json_google = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:nil];
            if ([_json_google[@"status"] isEqualToString:@"OK"]) {
                NSString *format_addr = _json_google[@"results"][0][@"formatted_address"];
                lat = _json_google[@"results"][0][@"geometry"][@"location"][@"lat"];
                lng = _json_google[@"results"][0][@"geometry"][@"location"][@"lng"];
                NSLog(@"get address=%@ lat=%@ and lng=%@\n", format_addr, lat, lng);
                CLLocationCoordinate2D droppin;
                droppin.latitude = (CLLocationDegrees)[lat doubleValue];//39.281516;
                droppin.longitude= (CLLocationDegrees)[lng doubleValue];//-76.580806;
                MKPointAnnotation *myPoint = [[MKPointAnnotation alloc] init];
                myPoint.coordinate = droppin;
                myPoint.title = format_addr;
                [mapView addAnnotation:myPoint];
            }else{
                counter--;
                NSLog(@"bad data counter=%d\n", counter);
                
            }
        }
    }
    
    if (counter == 0) {
        NSLog(@"ALERT ERROR\n");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Address cannot found!"
                                                        message:@"There is no location for this movie."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    //try to dequeue an existing pin view first.
    MKPinAnnotationView* pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"myPoint"];
    if (!pinView) {
        //if an existing pin view was not avilable, create one
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myPoint"];
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
    }
    else
        pinView.annotation = annotation;
    return pinView;
}

- (void)viewWillAppear:(BOOL)animated {
    // 1:37.778399, -122.420417
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 37.778399;//39.281516;
    zoomLocation.longitude= -122.420417;//-76.580806;
    
    // 2 (0.5, 0.5)
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 12*METERS_PER_MILE, 10*METERS_PER_MILE);
    
    // 3
    [mapView setRegion:viewRegion animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    addr_list = [[NSMutableArray alloc] init];
    
    NSString *urlAsString = [NSString stringWithFormat:@"http://localhost:8080/sfMovie/getLocation?title=%@", film_name];
    //NSString *urlAsString = @"https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyCBLL2U4fAvE6p3rC_j9KZqwOhrnPxCPcI";
    //NSString *urlAsString = @"https://maps.googleapis.com/maps/api/geocode/json?address=Tosca+Cafe,+San+Francisco,+CA&key=AIzaSyCBLL2U4fAvE6p3rC_j9KZqwOhrnPxCPcI";
    urlAsString = [urlAsString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSLog(@"url***%@\n", urlAsString);
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!theConnection) {
        server_connection = NO;
        NSLog(@"connection failed\n");
    }
    NSLog(@"map good connection\n");
    server_connection = YES;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
