//
//  AutoCompleteViewController.m
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import "AutoCompleteViewController.h"

@interface AutoCompleteViewController ()
{
    NSMutableArray *list;
    NSIndexPath *my_indexPath;
    BOOL using_name;
    BOOL using_director;
}

@end

@implementation AutoCompleteViewController
@synthesize textFeild, table, label;
#pragma mark UIButton Methods
- (IBAction)search_name:(id)sender{
    label.text = @"please enter film name\n";
    textFeild.text = nil;
    using_name = YES;
    using_director = NO;
}

#pragma mark NSURLConnectionDataDelegate methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSLog(@"didReceiveResponse\n");
    _responseData = [[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"didReceiveData\n");
    [_responseData appendData:data];
}

-(NSCachedURLResponse*)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    NSLog(@"willCacheResponse\n");
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"connectionDidFinishLoading\n");
    _json = [[NSDictionary alloc] init];
    _json = nil;
    if (_responseData) {
        _json = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:nil];
        //NSMutableArray *tmp = [[NSMutableArray alloc] init];
        list = [NSMutableArray arrayWithArray:_json[@"result"]];
        //NSString *tmp = _json[@"result"][0];
        for (int n=0; n<list.count; n++) {
            NSLog(@"list[%d]=%@\n", n, [list objectAtIndex:n]);
        }
        
        NSLog(@"get json data\n");
        [table reloadData];
    }
}

#pragma mark UITextFieldDelegate methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    textFeild.text = nil;
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    table.hidden = NO;
    NSString *substring = [NSString stringWithString:textFeild.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    
    //url call to get the autocompletion list
    NSLog(@"url call: sub_str=%@\n", substring);
    substring = [substring stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *urlAsString = [NSString stringWithFormat:@"http://localhost:8080/sfMovie/autoComplete?prefix=%@", substring];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!theConnection) {
        NSLog(@"connection failed\n");
    }
    NSLog(@"good connection\n");
    return YES;
    
}
#pragma mark UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //store indexpath
    my_indexPath = indexPath;
    //deselect the cell
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //get the cell and its text
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    textFeild.text = selectedCell.textLabel.text;
    
    //hide table
    tableView.hidden = YES;
    
    //jump to map
    //ViewController *jump = [self.storyboard instantiateViewControllerWithIdentifier:@"mapview"];
    //NotificationTableViewController *jump = [[NotificationTableViewController alloc] init];
    //[self.navigationController pushViewController:jump animated:YES];
    [self performSegueWithIdentifier:@"search2map" sender:nil];
}
#pragma mark UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return list.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ViewController *controller = (ViewController *)[segue destinationViewController];
    controller.film_name = [list objectAtIndex:my_indexPath.row];
    //controller. = [itemList objectAtIndex:cellPath.row];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    list = [[NSMutableArray alloc] init];
    /*NSString *item1 = @"item1";
    [list addObject:item1];
    NSString *item2 = @"item2";
    [list addObject:item2];
    NSString *item3 = @"item3";
    [list addObject:item3];
    NSString *item4 = @"item4";
    [list addObject:item4];*/
    
    table.hidden = YES;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
