//
//  ViewController.h
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, NSURLConnectionDataDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableString *film_name;

@end

