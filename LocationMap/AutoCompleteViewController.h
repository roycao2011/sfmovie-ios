//
//  AutoCompleteViewController.h
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AutoCompleteViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,NSURLConnectionDataDelegate>
{
    NSMutableData *_responseData;
    NSDictionary *_json;
    
}
@property (weak, nonatomic) IBOutlet UITextField *textFeild;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)search_name:(id)sender;



@end
