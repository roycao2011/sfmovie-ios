//
//  AppDelegate.h
//  LocationMap
//
//  Created by ZHOUYING on 12/6/14.
//  Copyright (c) 2014 ZHOUYING. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

