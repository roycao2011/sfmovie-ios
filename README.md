# README #

### What is this repository for? ###

* Quick summary
This is IOS frontend for SF movie service. (Uber's coding challenge)

* Version
1.0

### How do I get set up? ###

1. You need to have xcode on your mac.
2. Check out back end code here: https://roycao2011@bitbucket.org/roycao2011/sfmovie-jersy.git
3. Make sure that you started your local server in the last step:  just run:   mvn clean compile exec:java
4. Open project, Run 'Simulator' (target on 8.0 +).
5. There you go, enjoy this app.

### Contribution guidelines ###

You should be able to see a search box for matching movie titles. If you click any of them, then you will be directed to a map page, with each location for a drop pin.

### Who do I talk to? ###

Feel free to contact me at Roy Cao <silencetsao@gmail.com>